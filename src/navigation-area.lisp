(in-package :source-engine-nav-lib)
(export '(navigation-area identifier attribute-flag north-west-corner south-east-corner north-east-corner-height south-west-corner-height connections
	  navigation-area/read navigation-area/write
	  hiding-spot hiding-spot/read hiding-spot/write
	  approach-spot approach-spot/read approach-spot/write
	  encounter-path encounter-path/read encounter-path/write
	  custom-data))

(defclass navigation-area ()
  (
   (identifier :type (unsigned-byte 32) :initarg :identifier)
   (attribute-flag :type (unsigned-byte 32) :initarg :attribute-flag :documentation "Array of atribute bits.")
   (north-west-corner :type (cons single-float) :initarg :north-west-corner)
   (south-east-corner :type (cons single-float) :initarg :south-east-corner)
   (north-east-corner-height :type (cons single-float) :initarg :ne-corner-height)
   (south-west-corner-height :type (cons single-float) :initarg :sw-corner-height)
   (connections :type list :initform `( (north . ,(make-array 0 :element-type '(unsigned-byte 32) :adjustable t)) (east . ,(make-array 0 :element-type '(unsigned-byte 32) :adjustable t)) (south . ,(make-array 0 :element-type '(unsigned-byte 32) :adjustable t)) (west . ,(make-array 0 :element-type '(unsigned-byte 32) :adjustable t))) :initarg :connections) ; Association list of directions in CAR. Also, maybe make a short-hand accessor function to access the elements conveniently?
   (hiding-spots :type (array hiding-spot (256)) :initform (make-array 0 :element-type 'hiding-spot :adjustable t) :initarg :hiding-spots)
   (approach-spots :type (array approach-spot (256)) :initform (make-array 0 :element-type 'approach-spot :adjustable t) :initarg :approach-spots)
   (encounter-paths :type (array encounter-path (256)) :initform (make-array 0 :element-type 'encounter-path :adjustable t) :initarg :encounter-paths)
   (place-identifier :type (unsigned-byte 16) :initform 0 :initarg :place-identifier)
   (connected-ladders :type (cons (vector (unsigned-byte 32)) (vector (unsigned-byte 32))) :initform (cons (make-array 0 :adjustable t :element-type '(unsigned-byte 32)) (make-array 0 :adjustable t :element-type '(unsigned-byte 32))) :initarg :connected-ladders :documentation "(List) identifiers of ladders connected to this area. CAR is for upward ladders; CDR is for downard ladders.")
   (fastest-arrival-times :type (cons float float) :initform (cons 0.0 0.0) :documentation "Fastest time a bot from a team can arrive to this area. Added in NAV Version 8." :initarg :fastest-arrival-times)
   (corner-lights :type (vector single-float 4) :initform (make-array 4 :element-type 'single-float :initial-element 0.0) :documentation "Light Intensity in each corner.")
   (visibility-binds :type (vector (cons (unsigned-byte 32) unsigned-byte)) :initform (make-array 0 :element-type '(cons (unsigned-byte 32) unsigned-byte) :adjustable t) :documentation "Binds to areas determining their visibility.")
   (inherit-visiblity-area-id :type (unsigned-byte 32) :initform 0 :documentation "Area ID to inherit visibility from.")
   (custom-data :type list :initform nil :initarg :custom-data :documentation "Property list storing data used by different subversions of navigation-areas.")
   )
  )

(defgeneric navigation-area/read (version subversion navigation-area input-byte-stream)
  (:documentation "Modifies reads a navigation-area (according to base NAV format) from a byte-stream into navigation-area.

NOTE: To read game-variant navigation-areas, simply define a method with a subversion parameter equaling a value. e.g: (defmethod navigation-area/read (version (subversion (eql 'team-fortress-2)) (navigation-area navigation-area) input-byte-stream)")
  )

(defmethod navigation-area/read (version subversion (navigation-area navigation-area) input-byte-stream)
  "Modifies reads a navigation-area (according to base NAV format) from a byte-stream into navigation-area.

NOTE: To read game-variant navigation-areas, simply define a method with a subversion parameter equaling a value. e.g: (defmethod navigation-area/read (version (subversion (eql 'team-fortress-2)) (navigation-area navigation-area) input-byte-stream)"
  (declare (type (satisfies input-stream-p) input-byte-stream))

  (setf (slot-value navigation-area 'identifier) (read-integer-from-stream 4 input-byte-stream))
  (setf (slot-value navigation-area 'attribute-flag)
	(read-integer-from-stream
	 (cond ((<= version 8) 1) ((<= version 12) 2) (t 4))
	 input-byte-stream
	 )
	)
  (dolist (corner (list 'north-west-corner 'south-east-corner))
    (setf (slot-value navigation-area corner)
	  (mapcar #'decode-float32
		  (list (read-integer-from-stream 4 input-byte-stream)
			(read-integer-from-stream 4 input-byte-stream)
			(read-integer-from-stream 4 input-byte-stream))))
    )
  (setf (slot-value navigation-area 'north-east-corner-height) (decode-float32 (read-integer-from-stream 4 input-byte-stream)))
  (setf (slot-value navigation-area 'south-west-corner-height) (decode-float32 (read-integer-from-stream 4 input-byte-stream)))
  (dolist (direction (slot-value navigation-area 'connections))
    (let ((direction-list-size (read-integer-from-stream 4 input-byte-stream) ))
      (if (> direction-list-size 0) (setf (cdr direction) (make-sequence '(vector (unsigned-byte 32)) direction-list-size)))
      (dotimes (iterator direction-list-size ) (setf (aref (cdr direction) iterator) (read-integer-from-stream 4 input-byte-stream))))
    )
  
  (with-slots ((hsarray hiding-spots) (asarray approach-spots)) navigation-area
    (if (feature-enabled 'hiding-spots version)
      (let ((hsarr-length (read-byte input-byte-stream)))
	(if (> hsarr-length 0)
	  (list
	   (setf hsarray (make-array hsarr-length :element-type 'hiding-spot :initial-element (make-instance 'hiding-spot) :adjustable t))
	   (dotimes (iterator hsarr-length)
	    (hiding-spot/read version (aref hsarray iterator) input-byte-stream)))
	  ))
      )
    (if (feature-enabled 'approach-areas version)
      (let ((asarr-length (read-byte input-byte-stream)))
	(if (> asarr-length 0)
	  (list
	   (setf asarray (make-array asarr-length :adjustable t :element-type 'approach-spot :initial-element (make-instance 'approach-spot)))
	   (dotimes (iter asarr-length)
	    (approach-spot/read (aref asarray iter) input-byte-stream)))))
      )
    )
  (setf (slot-value navigation-area 'encounter-paths) (make-array (read-integer-from-stream 4 input-byte-stream) :element-type 'encounter-path :adjustable t))
  (dotimes (iter (length (slot-value navigation-area 'encounter-paths)))
    (setf (aref (slot-value navigation-area 'encounter-paths) iter) (make-instance 'encounter-path))
    (encounter-path/read (aref (slot-value navigation-area 'encounter-paths) iter) input-byte-stream)
    )
  
  (if (feature-enabled 'places version)
    (setf (slot-value navigation-area 'place-identifier) (read-integer-from-stream 2 input-byte-stream))
    (return-from navigation-area/read))
  
  (if (feature-enabled 'ladders version)
   (with-slots ((connected-ladders connected-ladders)) navigation-area
    ;; Array for up-facing connections.
    (let ((ladder-array-length (read-integer-from-stream 4 input-byte-stream)))
      (setf (car connected-ladders) (make-array ladder-array-length :adjustable t :element-type '(unsigned-byte 32) :initial-element 0))
    (dotimes (iter ladder-array-length)
      (setf (aref (car connected-ladders) iter) (read-integer-from-stream 4 input-byte-stream))
      ))
    ;; Array for down-facing connections.
    (let ((ladder-array-length (read-integer-from-stream 4 input-byte-stream)))
      (setf (cdr connected-ladders) (make-array ladder-array-length :adjustable t :element-type '(unsigned-byte 32) :initial-element 0))
    (dotimes (iter ladder-array-length)
      (setf (aref (cdr connected-ladders) iter) (read-integer-from-stream 4 input-byte-stream))
      ))
     )
    (return-from navigation-area/read)
    )

  (if (feature-enabled 'fastest-arrival-times version)
      (with-slots ((trav-times fastest-arrival-times)) navigation-area
    (setf
     (car trav-times) (decode-float32 (read-integer-from-stream 4 input-byte-stream))
     (cdr trav-times) (decode-float32 (read-integer-from-stream 4 input-byte-stream))))
    (return-from navigation-area/read))

  (if (feature-enabled 'corner-lights version)
    (with-slots ((corner-lights corner-lights)) navigation-area
      (dotimes (iter (length corner-lights))
	(setf (aref corner-lights iter) (decode-float32 (read-integer-from-stream 4 input-byte-stream)))))
    (return-from navigation-area/read)
    )
  
  (if (feature-enabled 'visibility-data version)
    (progn
      (with-slots ((visibility-binds visibility-binds)) navigation-area
    (setf visibility-binds (make-array (read-integer-from-stream 4 input-byte-stream) :element-type '(cons (unsigned-byte 32) unsigned-byte) :adjustable t))
    (dotimes (iter (length visibility-binds))
      (setf (aref visibility-binds iter) (cons (read-integer-from-stream 4 input-byte-stream) (read-byte input-byte-stream)))))
      (setf (slot-value navigation-area 'inherit-visiblity-area-id) (read-integer-from-stream 4 input-byte-stream)))
    (return-from navigation-area/read))
  )

(defgeneric navigation-area/write (version subversion navigation-area output-byte-stream)
  (:documentation "Write navigation-area in NAV format to a byte-stream.

NOTE: To write game-variant navigation-areas, simply define a method with a subversion parameter equaling a value. e.g: (defmethod navigation-area/write (version (subversion (eql 'team-fortress-2)) (navigation-area navigation-area) output-byte-stream)")
  )

(defmethod navigation-area/write (version subversion (navigation-area navigation-area) output-byte-stream)
  "Write navigation-area (according to base NAV format) to a byte-stream.

NOTE: To write game-variant navigation-areas, simply define a method with a subversion parameter equaling a value. e.g: (defmethod navigation-area/write (version (subversion (eql 'team-fortress-2)) (navigation-area navigation-area) output-byte-stream)"
  (declare (type (satisfies output-stream-p) output-byte-stream))

  (write-integer-to-stream (slot-value navigation-area 'identifier) 4 output-byte-stream)
  (write-integer-to-stream (slot-value navigation-area 'attribute-flag)
	     (cond ((<= version 8) 1) ((<= version 12) 2) (t 4))
	     output-byte-stream
	     )

  (with-slots ((corner1 north-west-corner) (corner2 south-east-corner) (necorner north-east-corner-height) (swcorner south-west-corner-height)) navigation-area
    (dolist (corner (list corner1 corner2))
	(mapcar (lambda (x) (write-integer-to-stream x 4 output-byte-stream))
		(mapcar #'encode-float32 corner))
      )
    (write-integer-to-stream (encode-float32 necorner) 4 output-byte-stream)
    (write-integer-to-stream (encode-float32 swcorner) 4 output-byte-stream)
    )
  ;; Write connections data.
  (dolist (direction (slot-value navigation-area 'connections))
    (write-integer-to-stream (length (cdr direction)) 4 output-byte-stream)
    (map 'nil (lambda (area-identifier) (write-integer-to-stream area-identifier 4 output-byte-stream)) (cdr direction))
    )
  ;; Write hiding spots
  (with-slots ((hsarray hiding-spots) (asarray approach-spots)) navigation-area
    (if (feature-enabled 'hiding-spots version)
      (list
	 (write-byte
	  (if (> (length hsarray) 0) (length hsarray) 0) output-byte-stream)
	 (map 'nil (lambda (spot) (hiding-spot/write version spot output-byte-stream)) (if (> (length hsarray) 0) hsarray nil))
	 )
      )
    (if (feature-enabled 'approach-areas version)
      (list
       (write-integer-to-stream
	(length asarray) 1 output-byte-stream)
       (map 'nil (lambda (spot) (approach-spot/write spot output-byte-stream)) (if (> (length asarray) 0) asarray nil)))
      )
    )

  (write-integer-to-stream (length (slot-value navigation-area 'encounter-paths)) 4 output-byte-stream)
  (dotimes (iter (length (slot-value navigation-area 'encounter-paths)))
    (encounter-path/write (aref (slot-value navigation-area 'encounter-paths) iter) output-byte-stream)
    )

  (if (feature-enabled 'places version)
    (write-integer-to-stream (slot-value navigation-area 'place-identifier) 2 output-byte-stream)
    (return-from navigation-area/write))
  ;; Ladders
  (if (feature-enabled 'ladders version)
    (dolist (ladder-array `(,(car (slot-value navigation-area 'connected-ladders)) ,(cdr (slot-value navigation-area 'connected-ladders))))
      (write-integer-to-stream (length ladder-array) 4 output-byte-stream)
      (dotimes (iter (length ladder-array))
	(write-integer-to-stream (aref ladder-array iter) 4 output-byte-stream)))
    (return-from navigation-area/write)
    )
  (if (feature-enabled 'fastest-arrival-times version)
   (with-slots ((trav-times fastest-arrival-times)) navigation-area
     (write-integer-to-stream (encode-float32 (car trav-times)) 4 output-byte-stream)
     (write-integer-to-stream (encode-float32 (cdr trav-times)) 4 output-byte-stream))
    (return-from navigation-area/write)
    )
  (if (feature-enabled 'corner-lights version)
    (with-slots ((corner-lights corner-lights)) navigation-area
      (dotimes (iter (length corner-lights))
	(write-integer-to-stream (encode-float32 (float (aref corner-lights iter))) 4 output-byte-stream)))
    (return-from navigation-area/write)
    )
  (if (feature-enabled 'visibility-data version)
    (progn
      (with-slots ((visibility-binds visibility-binds)) navigation-area
	(write-integer-to-stream (length visibility-binds) 4 output-byte-stream)
	(dotimes (iter (length visibility-binds))
	  (write-integer-to-stream (car (aref visibility-binds iter)) 4 output-byte-stream)
	  (write-byte (cdr (aref visibility-binds iter)) output-byte-stream)))
    (write-integer-to-stream (slot-value navigation-area 'inherit-visiblity-area-id) 4 output-byte-stream))
    (return-from navigation-area/write)
    )
  )

(defclass hiding-spot nil
  ((identifier :type (unsigned-byte 32) :initform 0 :initarg :identifier :documentation "Identifier. Added in NAV version 2.")
   (position :type (array single-float (3)) :initform (make-array 3 :element-type 'single-float :initial-element 0.0))
   (attribute-flag :type (unsigned-byte 8) :initform #x00 :documentation "Attribute flag. Added in NAV Version 2."))
  (:documentation "Represents NAV hiding spot. Added in NAV version 1.")
    )

(defmethod hiding-spot/read (version (hiding-spot hiding-spot) input-byte-stream)
  (declare (type (satisfies input-stream-p) input-byte-stream))
  (if (>= version
	  (getf (feature-plist-access 'hiding-spots) :added-attribute-flag))
    (setf (slot-value hiding-spot 'identifier) (read-integer-from-stream 4 input-byte-stream))
    )
  (if (feature-enabled 'hiding-spots version)
    (setf (slot-value hiding-spot 'position) (map '(array float (3)) #'decode-float32
						  (list
						   (read-integer-from-stream 4 input-byte-stream)
						   (read-integer-from-stream 4 input-byte-stream)
						   (read-integer-from-stream 4 input-byte-stream))) )
    )
  (if (>= version
	  (getf (feature-plist-access 'hiding-spots) :added-attribute-flag))
    (setf (slot-value hiding-spot 'attribute-flag) (read-byte input-byte-stream))
    )
  )

(defmethod hiding-spot/write (version (hiding-spot hiding-spot) output-byte-stream)
  (declare (type (satisfies output-stream-p) output-byte-stream))
  (if (>= version 2)
    (write-integer-to-stream (slot-value hiding-spot 'identifier) 4 output-byte-stream)
    )
  (if (feature-enabled 'hiding-spots version)
    (map 'nil (lambda (val) (write-integer-to-stream (encode-float32 val) 4 output-byte-stream)) (slot-value hiding-spot 'position))
    )
  (if (>= version 2)
    (write-byte (slot-value hiding-spot 'attribute-flag) output-byte-stream)
    )
  )

(defclass approach-spot nil
  (
   (target-identifier :type (unsigned-byte 32) :initform 0)
   (previous-identifier :type (unsigned-byte 32) :initform 0)
   (type :type (unsigned-byte 8) :initform 0)
   (next-identifier :type (unsigned-byte 32) :initform 0)
   (how :type (unsigned-byte 8) :initform 0 :initarg :how)
   )
  (:documentation "NAV Approach spot. Removed in NAV version 15."))

(defmethod approach-spot/read ((approach-spot approach-spot) input-byte-stream)
  (declare (type (satisfies input-stream-p) input-byte-stream))
  (setf (slot-value approach-spot 'target-identifier) (read-integer-from-stream 4 input-byte-stream))
  (setf (slot-value approach-spot 'previous-identifier) (read-integer-from-stream 4 input-byte-stream ))
  (setf (slot-value approach-spot 'type) (read-integer-from-stream 1 input-byte-stream ))
  (setf (slot-value approach-spot 'next-identifier) (read-integer-from-stream 4 input-byte-stream ))
  (setf (slot-value approach-spot 'how) (read-integer-from-stream 1 input-byte-stream ))
  )

(defmethod approach-spot/write ((approach-spot approach-spot) output-byte-stream)
  (declare (type (satisfies output-stream-p) output-byte-stream))
  (write-integer-to-stream (slot-value approach-spot 'target-identifier) 4 output-byte-stream )
  (write-integer-to-stream (slot-value approach-spot 'previous-identifier) 4 output-byte-stream )
  (write-integer-to-stream (slot-value approach-spot 'type) 1 output-byte-stream )
  (write-integer-to-stream (slot-value approach-spot 'next-identifier) 4 output-byte-stream )
  (write-integer-to-stream (slot-value approach-spot 'how) 1 output-byte-stream )
  )

(defclass encounter-path nil
  ((entry :type (cons (unsigned-byte 32) symbol) :initform (cons 0 'north) :documentation "An AreaID-Direction pair that signifies the entry from another area.")
  (destination :type (cons (unsigned-byte 32) symbol) :initform (cons 0 'north) :documentation "An AreaID-Direction pair that signifies the movement into another area.")
   (spots :type (or (cons (cons (unsigned-byte 32) (float 0.0 1.0))) null) :initform '() :documentation "Spots (in AreaID and parametric distance pairs) for the bot to travel through."))
  (:documentation "NAV Encounter path.")
  )


(defmethod encounter-path/read ((encounter-path encounter-path) input-byte-stream)
  (declare (type stream input-byte-stream))
  (dolist (par '(entry destination))
    (setf (car (slot-value encounter-path par)) (read-integer-from-stream 4 input-byte-stream))
    (setf (cdr (slot-value encounter-path par)) (car (rassoc (read-byte input-byte-stream) direction-enum))))
  (with-slots ((spots spots)) encounter-path
    (let ((amount (read-byte input-byte-stream)))
      (setf spots (mapcar
		   (lambda (x)
		     (cons (read-integer-from-stream 4 input-byte-stream)
			   (let ((parametric-distance (float (read-byte input-byte-stream))))
				   (if (= parametric-distance 0.0) 0.0 (expt parametric-distance -1))))) (make-list amount)))))
  )

(defmethod encounter-path/write ((encounter-path encounter-path) output-byte-stream)
  (declare (type stream output-byte-stream))
  (dolist (par '(entry destination))
    (write-integer-to-stream (car (slot-value encounter-path par)) 4 output-byte-stream)
    (write-byte (cdr (assoc (cdr (slot-value encounter-path par)) direction-enum)) output-byte-stream))
  (with-slots ((spots spots)) encounter-path
    (write-byte (length spots) output-byte-stream)
    (map 'nil (lambda (espot)
		(write-integer-to-stream (car espot) 4 output-byte-stream)
		;; The parametric distance is stored as a inverse of the float.
		(write-byte (floor (expt (cdr espot) -1)) output-byte-stream))
	 spots))
  )
