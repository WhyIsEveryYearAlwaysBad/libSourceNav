(in-package :source-engine-nav-lib)
(export '(navigation-ladder navigation-ladder/read navigation-ladder/write
	  identifier
	  position width top-corner bottom-corner length 
	  area-connections top-forward top-left top-right top-back bottom
	  ladder-direction-enum))

(defconstant ladder-direction-enum
  '( (up . 0) (down . 1) )
  "Maps ladder direction to an unsigned integer.")

(defclass navigation-ladder nil
  (
   (identifier :type (unsigned-byte 32) :initform 0 :initarg :identifier)
   (position :type list :initform `(width 0.0 top-corner #(0.0 0.0 0.0) bottom-corner #(0.0 0.0 0.0) length 0.0 direction up) :initarg :position)
   (area-connections :type list :initform `(top-forward 0 top-left 0 top-right 0 top-back 0 bottom 0) :initarg :area-connections)
   )
  (:documentation "Navigation area for ladders. Added in NAV Version 6."))

(defmethod navigation-ladder/read ((navigation-ladder navigation-ladder) input-byte-stream)
  (declare (type (satisfies input-stream-p) input-byte-stream))
  (setf (slot-value navigation-ladder 'identifier) (read-integer-from-stream 4 input-byte-stream))
  (with-slots ((position position)) navigation-ladder
    (setf
     (getf position 'width) (decode-float32 (read-integer-from-stream 4 input-byte-stream))
    
     (getf position 'top-corner)
     (map '(array float (3)) #'decode-float32 (list (read-integer-from-stream 4 input-byte-stream)
								(read-integer-from-stream 4 input-byte-stream)
								(read-integer-from-stream 4 input-byte-stream)))
     (getf position 'bottom-corner)
     (map '(array float (3)) #'decode-float32 (list (read-integer-from-stream 4 input-byte-stream)
								(read-integer-from-stream 4 input-byte-stream)
								(read-integer-from-stream 4 input-byte-stream)))
     (getf position 'length) (decode-float32 (read-integer-from-stream 4 input-byte-stream))
     (getf position 'direction) (car (rassoc (read-integer-from-stream 4 input-byte-stream) ladder-direction-enum))
     )
    )
  (dolist (prop '(top-forward top-left top-right top-back bottom))
    (setf (getf (slot-value navigation-ladder 'area-connections) prop) (read-integer-from-stream 4 input-byte-stream))
    )
  )

(defmethod navigation-ladder/write ((navigation-ladder navigation-ladder) output-byte-stream)
  (declare (type (satisfies output-stream-p) output-byte-stream))
  (source-engine-nav-lib::write-integer-to-stream (slot-value navigation-ladder 'identifier) 4 output-byte-stream)
  (with-slots ((position position)) navigation-ladder
    (write-integer-to-stream (encode-float32 (getf position 'width)) 4 output-byte-stream)
    
    (map 'nil (lambda (val) (write-integer-to-stream (encode-float32 val) 4 output-byte-stream)) (getf position 'top-corner))
    (map 'nil (lambda (val) (write-integer-to-stream (encode-float32 val) 4 output-byte-stream)) (getf position 'bottom-corner))
    (write-integer-to-stream (encode-float32 (getf position 'length)) 4 output-byte-stream)
    (write-integer-to-stream (cdr (assoc (getf position 'direction) ladder-direction-enum)) 4 output-byte-stream)
    )
  (dolist (prop '(top-forward top-left top-right top-back bottom))
    (write-integer-to-stream (getf (slot-value navigation-ladder 'area-connections) prop) 4 output-byte-stream)
    )
  )
