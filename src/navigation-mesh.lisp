;;;; LISP Code for CNavMesh.
(in-package :source-engine-nav-lib)
(export '(navigation-mesh version subversion bsp-file-size analyzedp place-list allow-undefined-places-p area-array ladder-array
	  navigation-mesh/read navigation-mesh/write))


(defclass navigation-mesh ()
  (
   (version
    :type (unsigned-byte 32)
    :initform 0
    :initarg :version
    :documentation "Version of the base NAV file format. Default value is 0."
    )
   (subversion
    :type (unsigned-byte 32)
    :initarg :subversion
    :documentation "Version of the derivative NAV file format."
    )
   (bsp-file-size
    :type (unsigned-byte 32)
    :initform 0
    :initarg :bsp-file-size
    :documentation "Byte size of a BSP file. The navigation mesh is autoloaded if this variable matches the file size of the loaded BSP."
    )
   (analyzedp
    :type bit
    :initform 0
    :initarg :analyzedp
    :documentation "Default value: 0

analyzedp is a boolean that is set to 1 if the navigation mesh has been analyzed (through `nav_analyze`). It was introduced in NAV version 14.

NOTE: analyzedp is stored as a *byte* in the NAV format."
    )
   (place-list
    :type (or (cons string) null)
    :initform '()
    :initarg :place-list
    :documentation "A list of labels to mark with navigation meshes."
    )
   (allow-undefined-places-p
    :type bit
    :initform 0
    :initarg :allow-undefined-places-p
    :documentation "allow-undefined-places-p is a boolean (as a byte) that determines if undefined places can exist.

NOTE: This variable is stored as a byte in the NAV file format."
    )
   (area-array
    :type (vector navigation-area)
    :initform (make-array 0 :adjustable t :element-type 'navigation-area)
    :initarg :area-array
    )
   (ladder-array
    :type (vector navigation-ladder)
    :initform (make-array 0 :adjustable t :element-type 'navigation-ladder)
    :initarg :ladder-array
    )
   )
  (:documentation "Class interface for the base (in NAV) navigation mesh.")
  )

(defgeneric navigation-mesh/read (navigation-mesh input-byte-stream &optional &key section)
  (:documentation "A generic function that modifingly reads navigation mesh from a binary stream. A section can be specified to determine what section of data will be read.")
  )

(defmethod navigation-mesh/read ((navigation-mesh navigation-mesh) input-byte-stream &optional &key section)
  "A specialized method that reads base-NAV data into (the) navigation(-)mesh.

If :section is :header, the method will only read in the header of the binary stream."
  (declare (type (satisfies input-stream-p) input-byte-stream))

  ;; Read the header (and only that if section is :header).
  (let ((signature-buffer (list 0 0 0 0) ))
    (read-sequence signature-buffer input-byte-stream)
    (if (not (equalp signature-buffer format-signature))
      (return-from navigation-mesh/read (signal (make-condition 'simple-condition :format-control "Stream byte-array signature does not match the expected byte-array signature. ~S (expected) != ~S (actual)" :format-arguments `(format-signature signature-buffer))))
      )
    )
  (with-slots ((version version)) navigation-mesh
    (setf version (read-integer-from-stream 4 input-byte-stream))
    (if (feature-enabled 'subversion version)
      (progn
	(setf (slot-value navigation-mesh 'subversion) (read-integer-from-stream 4 input-byte-stream))
	)))
  (if (eq section :header)
    (return-from navigation-mesh/read)
    )
  
  (with-slots (version) navigation-mesh
    ;; Read the BSP file size.
    (if (feature-enabled 'bsp-file-size version)
	(setf (slot-value navigation-mesh 'bsp-file-size) (read-integer-from-stream 4 input-byte-stream))
	)

    ;; Read the analyzedp boolean.
    (if (feature-enabled 'analysis-mark version)
	(setf (slot-value navigation-mesh 'analyzedp) (read-byte input-byte-stream))
	)

    (if (feature-enabled 'places version)
	(with-slots (place-list) navigation-mesh
	  (setf place-list (make-sequence 'list (read-integer-from-stream 2 input-byte-stream) :initial-element ""))
	  (dotimes (index (length place-list)) ;; Read each place name string and append them to the place-list.
	    (let ((place-string-bytes (make-sequence 'list (read-integer-from-stream 2 input-byte-stream) :initial-element 0)))
	      (read-sequence place-string-bytes input-byte-stream)
	      (setf (nth index place-list) (map 'string #'code-char place-string-bytes))
	      )
	    )
	  )
	)

    ;; Read the has named areas boolean.
    (if (feature-enabled 'unnamed-areas version)
	(setf (slot-value navigation-mesh 'allow-undefined-places-p) (read-byte input-byte-stream))
      )
    ;; Read areas.
    (with-slots ((area-array area-array) version subversion) navigation-mesh
      (setf area-array (make-array (read-integer-from-stream 4 input-byte-stream) :element-type 'navigation-area :adjustable t))
      (dotimes (iter (length area-array))
	(setf (aref area-array iter) (make-instance 'navigation-area))
	(navigation-area/read version subversion (aref area-array iter) input-byte-stream)
	)
      )

    (if (feature-enabled 'ladders version)
      (with-slots ((ladder-array ladder-array)) navigation-mesh
	(let ((amount (read-integer-from-stream 4 input-byte-stream)))
	    (setf ladder-array
	      (map-into
	       (make-array (list amount) :element-type 'navigation-ladder :initial-element (make-instance 'navigation-ladder) :adjustable t)
	       (lambda () (navigation-ladder/read (make-instance 'navigation-ladder) input-byte-stream))
	       )))
	)
      )
    )
  )

(defgeneric navigation-mesh/write (navigation-mesh output-byte-stream &optional &key section)
  (:documentation "A generic function that writes a navigation mesh to a binary stream. A section can be specified to determine what section of data will be written.")
  )

(defmethod navigation-mesh/write ((navigation-mesh navigation-mesh) output-byte-stream &optional &key section)
  "A specialized method for the base navigation mesh that writes the navigation mesh, in the base NAV format, to a binary stream.

If :section is :header, the method will only write the header of the navigation-mesh."
  (declare (type (satisfies output-stream-p) output-byte-stream))
  ;; Write NAV header.
  (write-sequence format-signature output-byte-stream)
  (with-slots (version) navigation-mesh
    (write-integer-to-stream version 4 output-byte-stream)
    (if (feature-enabled 'subversion version)
	(write-integer-to-stream (slot-value navigation-mesh 'subversion) 4 output-byte-stream)
	)
    )
  (if (eq section :header)
    (return-from navigation-mesh/write)
    )
  ;; Write NAV Data.
  (with-slots (version subversion) navigation-mesh
    (if (feature-enabled 'bsp-file-size version)
	(write-integer-to-stream (slot-value navigation-mesh 'bsp-file-size) 4 output-byte-stream))

    (if (feature-enabled 'analysis-mark version)
      (write-byte (slot-value navigation-mesh 'analyzedp) output-byte-stream))

    (if (feature-enabled 'places version)
	(with-slots (place-list) navigation-mesh
	  (write-integer-to-stream (length place-list) 2 output-byte-stream)
	  (map nil
	       (lambda (place-name)
		 (write-integer-to-stream (length place-name) 2 output-byte-stream)
		 (write-sequence (map 'list #'char-code place-name) output-byte-stream))
	       place-list)
	  ))
    (if (feature-enabled 'corner-lights version)
	(write-byte (slot-value navigation-mesh 'allow-undefined-places-p) output-byte-stream)
      )

    (with-slots ((area-array area-array)) navigation-mesh
      (write-integer-to-stream (length area-array) 4 output-byte-stream)
      (dotimes (iter (length area-array))
	(navigation-area/write version subversion (aref area-array iter) output-byte-stream)
	)
      )

    (if (feature-enabled 'ladders version)
      (with-slots ((ladder-array ladder-array)) navigation-mesh
	(write-integer-to-stream (length ladder-array) 4 output-byte-stream)
	(dotimes (iter (length ladder-array))
	  (navigation-ladder/write (aref ladder-array iter) output-byte-stream)
	  )
	)
      )
    )
  )
