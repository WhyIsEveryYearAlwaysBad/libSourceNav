(in-package :source-engine-nav-lib)

(defpackage "SOURCE-ENGINE-NAV-LIB/NAVIGATION-MESH/TEST"
  (:use :cl :source-engine-nav-lib)
  )

;; Test the base header-read and header-write functions to ensure that they write properly.

(defun test-header-io (ver subver)
  (let (
      (source-navigation-mesh (make-instance 'navigation-mesh :version ver :subversion subver :place-list `("asdf" "temp")))
      (target-navigation-mesh (make-instance 'navigation-mesh))
      (written-format-signature/byte-array '(0 0 0 0))
      )

  (with-open-file (temp-str "/tmp/header-test" :direction :io :element-type 'unsigned-byte :if-exists :supersede)
    (handler-bind ((simple-condition #'invoke-debugger))
      (navigation-mesh/header-write source-navigation-mesh temp-str)
      (navigation-mesh/data-write source-navigation-mesh temp-str)
      ;; Store the written format signature for verification.
      (file-position temp-str 0)
      (read-sequence written-format-signature/byte-array temp-str)
      (file-position temp-str 0)
      (navigation-mesh/header-read target-navigation-mesh temp-str)
      (navigation-mesh/data-read target-navigation-mesh temp-str)
    ;; NOTE: I have no idea how to make a stream without a file.
      ;; (delete-file temp-str)
      )
    )
  (assert (equalp written-format-signature/byte-array format-signature) (written-format-signature/byte-array) "The written format signature does not match the expected format signature!

Written Signature: ~A
Expected signature: ~A" written-format-signature/byte-array format-signature)
    ;; Verify that the version number is properly read and properly written.
    (assert (equal
	     (slot-value source-navigation-mesh 'version)
	     (slot-value target-navigation-mesh 'version)
	     ) () "Version values of the nav meshes are not equal!

Written version integer: ~S
Read version integer: ~S" (slot-value source-navigation-mesh 'version)
(slot-value target-navigation-mesh 'version)
)
    ;; Verify that the subversion number is properly read and properly written.
    (if (>= ver 10)
	(assert (eq (slot-value source-navigation-mesh 'subversion) (slot-value source-navigation-mesh 'subversion))
		((slot-value source-navigation-mesh 'subversion) (slot-value target-navigation-mesh 'subversion))
		"Subversion values of the nav meshes are not equal!")
	)
    ;; Verify that the BSP file size is properly read and properly written.
    (assert (equal
	     (slot-value source-navigation-mesh 'bsp-file-size)
	     (slot-value target-navigation-mesh 'bsp-file-size)
	     ) () "BSP file sizes of the nav meshes are not equal!

Written bsp-file-size integer: ~S
Read bsp-file-size integer: ~S" (slot-value source-navigation-mesh 'bsp-file-size) (slot-value target-navigation-mesh 'bsp-file-size))
    (if (>= ver 14)
	;; Verify that `analyzedp` is properly read and properly written.
	(assert (equal
		 (slot-value source-navigation-mesh 'analyzedp)
		 (slot-value target-navigation-mesh 'analyzedp)
		 ) () "The analyzed boolean of the nav meshes are not equal!")
	)
    
    (if (>= ver 5)
	;; Verify that the place-list is properly read and properly written.
	(assert (equal
		 (slot-value source-navigation-mesh 'place-list)
		 (slot-value target-navigation-mesh 'place-list)
		 ) () "Place names of the nav meshes are not equal!")
	)

    (if (> ver 11)
	(assert (equal (slot-value source-navigation-mesh 'allow-undefined-places-p)
		       (slot-value target-navigation-mesh 'allow-undefined-places-p))
		( (slot-value source-navigation-mesh 'allow-undefined-places-p) (slot-value target-navigation-mesh 'allow-undefined-places-p))"`allow-undefined-places-p` in both navigation meshes are not equal!" )
      )
    (if (>= ver 7)
      (assert (equalp (slot-value source-navigation-mesh 'ladder-array)
		       (slot-value target-navigation-mesh 'ladder-array))
		( (slot-value source-navigation-mesh 'ladder-array) (slot-value target-navigation-mesh 'ladder-array))"ladder-array` in both navigation meshes are not equal!" )
      )
    )
  )

;; Test the base header IO for every version and subversion combination that is used.
(dotimes (version 16)
  (dotimes (subver 14)
    (test-header-io version subver)
    )
  )

; (delete-package 'source-engine-nav-lib::source-engine-nav-lib/navigation-mesh/test)
