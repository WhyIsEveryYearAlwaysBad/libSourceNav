(in-package :source-engine-nav-lib)

(defpackage "SOURCE-ENGINE-NAV-LIB/NAVIGATION-AREA/TEST"
  (:use :cl :source-engine-nav-lib)
  )

(defun test-header-io (nav-vers subvers)
  (let (
	(source-navigation-area (make-instance 'navigation-area :identifier (random #xFFFFFFFF)
								:attribute-flag (random (cond ((<= nav-vers 8) #xFF)
										   ((<= nav-vers 12) #xFFFF)
										   (t #xFFFFFFFF)))
								:north-west-corner '(1.3 1.2 1.4)
								:south-east-corner '(-1.3 -1.2 -1.4)
								:ne-corner-height (- (random 20.5))
								:sw-corner-height (random 75.23)
								:connections '((north . #())
									       (east . #(342 32))
									       (south . #())
									       (west . #(912)))
								;:hiding-spots (make-array (random 10) :element-type (find-class 'hiding-spot) :initial-element (make-instance 'hiding-spot :identifier (random (1- (expt 2 32)))))
								:approach-spots (make-array (random 10) :element-type 'approach-spot :adjustable t :initial-element (make-instance 'approach-spot :how (random (1- (expt 2 8)))))
								:encounter-paths (make-array (random 4) :element-type 'encounter-path :adjustable t :initial-element (make-instance 'encounter-path))
								:connected-ladders (cons (make-array 0 :adjustable t :element-type '(unsigned-byte 32)) (make-array (random 3) :adjustable t :initial-element (random (1- (expt 2 32))) :element-type '(unsigned-byte 32)))
								:fastest-arrival-times (cons (random 10.0) (random 10.0))
								))
	(target-navigation-area (make-instance 'navigation-area))
	)

    (with-open-file (temp-str "/tmp/nav-area-test" :direction :io :element-type 'unsigned-byte :if-exists :supersede)
      (handler-bind ((simple-condition #'invoke-debugger))
	(navigation-area/write nav-vers subvers source-navigation-area temp-str)
	;; Store the written format signature for verification.
	(file-position temp-str 0)
	(navigation-area/read nav-vers subvers target-navigation-area temp-str)
	;; NOTE: I have no idea how to make a stream without a file.
	;; (delete-file temp-str)
	)
      )
    (dolist (slot '(identifier attribute-flag north-west-corner south-east-corner north-east-corner-height south-west-corner-height connections place-identifier)) 
      (assert (equalp (slot-value source-navigation-area slot) (slot-value target-navigation-area slot)) (nav-vers slot) "The written slot ~S does not match the expected slot!

Written: ~A
Read: ~A" (write-to-string slot) (slot-value source-navigation-area slot) (slot-value target-navigation-area slot))
      ;; Verify that the version number is properly read and properly written.     
      )
    (if (>= nav-vers 1)
      (macrolet ((index (nav-area) "" `(aref (slot-value ,nav-area 'hiding-spots) iter)))
	(dotimes (iter (length (slot-value source-navigation-area 'hiding-spots)))
	  (dolist (hsslot (append '(position) (if (>= nav-vers 2) (list 'identifier 'attribute-flag))))
	   (assert (equalp (slot-value (index source-navigation-area) hsslot) (slot-value (index target-navigation-area) hsslot)) (nav-vers hsslot) "The written hiding spot ~S does not match the expected slot!

Written: ~A
Read: ~A" (write-to-string hsslot) (slot-value (index source-navigation-area) hsslot) (slot-value (index target-navigation-area) hsslot)))))
      )
    (if (< nav-vers 15)
      (macrolet ((index (nav-area) "" `(aref (slot-value ,nav-area 'approach-spots) iter)))
	(dotimes (iter (length (slot-value source-navigation-area 'approach-spots)))
	  (dolist (hsslot '(TARGET-IDENTIFIER PREVIOUS-IDENTIFIER TYPE NEXT-IDENTIFIER HOW))
	   (assert (equalp (slot-value (index source-navigation-area) hsslot) (slot-value (index target-navigation-area) hsslot)) (nav-vers hsslot) "The written approach spot ~S does not match the expected slot!

Written: ~A
Read: ~A" (write-to-string hsslot) (slot-value (index source-navigation-area) hsslot) (slot-value (index target-navigation-area) hsslot)))))
      )
      (macrolet ((index (nav-area) "" `(aref (slot-value ,nav-area 'encounter-paths) iter)))
	(dotimes (iter (length (slot-value source-navigation-area 'encounter-paths)))
	  (dolist (sslot '(entry destination spots))
	   (assert (equalp (slot-value (index source-navigation-area) sslot) (slot-value (index target-navigation-area) sslot)) (nav-vers sslot) "The written approach spot ~S does not match the expected slot!

Written: ~A
Read: ~A" (write-to-string sslot) (slot-value (index source-navigation-area) sslot) (slot-value (index target-navigation-area) sslot)))))
    (if (> nav-vers 6)
      (assert (equalp (slot-value source-navigation-area 'connected-ladders) (slot-value target-navigation-area 'connected-ladders)) (nav-vers) "The written slot ~S does not match the expected slot!

Written: ~A
Read: ~A" (write-to-string 'connected-ladders) (slot-value source-navigation-area 'connected-ladders) (slot-value target-navigation-area 'connected-ladders))
      )
    (if (>= nav-vers 8)
      (assert (equalp (slot-value source-navigation-area 'fastest-arrival-times) (slot-value target-navigation-area 'fastest-arrival-times)) (nav-vers) "The written slot ~S does not match the expected slot!

Written: ~A
Read: ~A" (write-to-string 'fastest-arrival-times) (slot-value source-navigation-area 'fastest-arrival-times) (slot-value target-navigation-area 'fastest-arrival-times))
      )
    (if (>= nav-vers 11)
      (assert (equalp (slot-value source-navigation-area 'corner-lights) (slot-value target-navigation-area 'corner-lights)) (nav-vers) "The written slot ~S does not match the expected slot!

Written: ~A
Read: ~A" (write-to-string 'corner-lights) (slot-value source-navigation-area 'corner-lights) (slot-value target-navigation-area 'corner-lights))
      )
    (if (>= nav-vers 16)
      (assert (equalp (slot-value source-navigation-area 'visibility-binds) (slot-value target-navigation-area 'visibility-binds)) (nav-vers) "The written slot ~S does not match the expected slot!

Written: ~A
Read: ~A" (write-to-string 'visibility-binds) (slot-value source-navigation-area 'visibility-binds) (slot-value target-navigation-area 'visibility-binds))
      (assert (equalp (slot-value source-navigation-area 'inherit-visiblity-area-id) (slot-value target-navigation-area 'inherit-visiblity-area-id)) (nav-vers) "The written slot ~S does not match the expected slot!

Written: ~A
Read: ~A" (write-to-string 'inherit-visiblity-area-id) (slot-value source-navigation-area 'inherit-visiblity-area-id) (slot-value target-navigation-area 'inherit-visiblity-area-id))
      )
    )
  )

(dotimes (nav-vers 16)
  (dotimes (subvers 16)
   (test-header-io nav-vers subvers))
  )

(delete-package "SOURCE-ENGINE-NAV-LIB/NAVIGATION-AREA/TEST")
