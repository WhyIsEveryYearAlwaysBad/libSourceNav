(in-package :source-engine-nav-lib)

(export '(format-signature direction-enum feature-version-map feature-enabled game-subversion-map area-attribute-list))

(defconstant format-signature '(#xCE #xFA #xED #xFE) "This sequence of bytes is placed at the beginning of a NAV file.")

(defconstant direction-enum
  '( (north . 0) (east . 1) (south . 2) (west . 3) )
  "Maps the NAV byte value to the direction; similar to the NavDirType enum."
  )

(defconstant feature-version-map
  '(
    (hiding-spots . (:added 1 :added-identifier 2 :added-attribute-flag 2))
    (bsp-file-size . (:added 4))
    (places . (:added 5))
    (ladders . (:added 7))
    (fastest-arrival-times . (:added 8))
    (area-attribute-flag-size . 
     ( (8 . 0) (16 . 9) (32 . 13)  ))
    (subversion . (:added 10))
    (corner-lights . (:added 11))
    (unnamed-areas . (:added 12))
    (analysis-mark . (:added 14))
    (approach-areas . (:added 0 :removed 15))
    (visibility-data . (:added 16))
    )
  "Association list of pairs between features and plists describing changes relative to mesh versions.

The association list consists of a CONS with asymbol representing a feature in CAR; then in CDR the property list to describe the version changes in relation to that feature."
  )

(export `,(mapcar #'car feature-version-map) )

(defun feature-plist-access (feature-symbol)
  (cdr (assoc feature-symbol feature-version-map))
  )

(defun feature-enabled (feature-symbol mesh-version &aux (feature (assoc feature-symbol feature-version-map)))
  "Short hand function for testing if a feature in a nav-mesh would be present.

Returns T if true, nil otherwise. Error will be given if :added is not present."
  (cond
    ((not feature) (error "Feature ~S was not present in ~S!" feature-symbol 'feature-version-map))
    ((null (get-properties (cdr feature) '(:added)))
	 (error ":added is not present in feature ~s; which is required to determine existance in a nav-mesh version." 'feature feature))
    (t (if  ;; :added is present then:
	(and (>= mesh-version (getf (cdr feature) :added) )
	     ;; If :removed was present, check if the feature would be deleted.
	     (if (get-properties (cdr feature) '(:removed))
	       `,(< mesh-version (getf (cdr feature) :removed))
	       t)
	     )
      t 					; If true, then feature is in that mesh version.
      nil					; otherwise, feature is not in that mesh version.
      ))
    )
  )

(defconstant game-subversion-map
  '(
    (base . 0)			     ; Base nav mesh implementation.
    (tf2 . 2) 				; Team Fortress 2
    (csgo . 1)				; Counter-Strike Global Offensive
    (css . 1)			; Counter-Strike Source
    (l4d . 13)			; Left 4 Dead
    (l4d2 . 14)			; Left 4 Dead 2
    )
  )

(defconstant area-attribute-list
  '(
    (:CROUCH . (:bit-position 0))
    (:JUMP . (:bit-position 1))
    (:PRECISE . (:bit-position 2))
    (:NO-JUMP . (:bit-position 3))
    (:STOP . (:bit-position 4))
    (:RUN . (:bit-position 5))
    (:WALK . (:bit-position 6))
    (:AVOID . (:bit-position 7))
    (:TRANSIENT . (:bit-position 8))
    (:DONT-HIDE . (:bit-position 9))
    (:STAND . (:bit-position 10))
    (:NO-HOSTAGES . (:bit-position 11))
    (:STAIRS . (:bit-position 12))
    (:NO-MERGE . (:bit-position 13))
    (:OBSTACLE-TOP . (:bit-position 14))
    (:CLIFF . (:bit-position 15))
    (:FIRST-CUSTOM . (:bit-position 16))
    (:LAST-CUSTOM . (:bit-position 26))
    (:FUNC-COST . (:bit-position 27))
    (:HAS-ELEVATOR . (:bit-position 28))
    (:NAV-BLOCKER . (:bit-position 29))
    )
  "List of area attributes used by the Source Engine to determine Bot behaviors for areas.")
