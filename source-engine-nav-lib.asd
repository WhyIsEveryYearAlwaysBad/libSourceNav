(defpackage #:source-engine-nav-lib
  (:use :cl)
  (:documentation "A library for interfacing with the NAV file format in the Source Engine.")
  (:nicknames nav)
  )

(asdf:defsystem "source-engine-nav-lib"
  :version "0.0.1"
  :description "A library for interfacing with the NAV file format in the Source Engine."
  :license "AGPL"
  :pathname "src/"
  :components ((:module "base"
		:pathname ""
		:components ((:file "utilities")
			     (:file "format")
			     (:file "navigation-area" :depends-on ("format" "utilities"))
			     (:file "navigation-ladder" :depends-on ("format" "utilities"))
			     (:file "navigation-mesh" :depends-on ("format" "utilities" "navigation-area" "navigation-ladder")))))
  )
